class ChangeDatatypeAgeOfUsers < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :age, :string
  end
end
