class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :phone_number
      t.string :purpose
      t.string :level
      t.string :season
      t.string :preference

      t.timestamps
    end
  end
end
