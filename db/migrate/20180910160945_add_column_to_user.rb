class AddColumnToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :area_preference, :string
    add_column :users, :school_level, :string
    add_column :users, :school_type, :string
    add_column :users, :school_scale, :string
  end
end
