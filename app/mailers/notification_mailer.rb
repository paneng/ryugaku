class NotificationMailer < ApplicationMailer
  def notice(user)
    @user = user
    mail(
      subject: 'LPからCV発生', #メールのタイトル,
      to: 'info.connectflow@gmail.com', #宛先
    ) do |format|
      format.html
    end
  end
end
