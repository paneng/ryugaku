class StepsController < ApplicationController
  include Wicked::Wizard
  steps :second, :thanks, :thanks2

  def new
    @user = User.new
    render "new"
  end

  def show
    @user = current_user
    render_wizard
  end

  def update
    @user = current_user
    @user.attributes = user_params
    render_wizard @user
  end

  private

  def finish_wizard_path
    steps_thanks_path
  end

  def current_user
    User.find_by(id: session[:user_id])
  end

  def user_params
    params.require(:user).permit(:purpose, :level, :name, :phone_number, :season, :email, :preference, :age, :area_preference, :school_level, :school_type, :school_scale, :japnum)
  end
end
