class UsersController < ApplicationController
  before_action :basic_auth, only: [:index, :show, :edit, :destroy]
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @user = User.all.order('id DESC')
    # render :layout => 'users'
  end

  def show
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      NotificationMailer.notice(@user).deliver_now
      redirect_to steps_path
    else
      render "steps/new"
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def basic_auth
    authenticate_or_request_with_http_basic do |name, password|
      name == ENV['ADMIN_LOGIN_NAME'] && password == ENV['ADMIN_LOGIN_PASSWORD']
    end
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:purpose, :level, :name, :phone_number, :season, :email, :preference, :age, :area_preference, :school_level, :school_type, :school_scale, :japnum)
  end
end
