Rails.application.routes.draw do
  resources :users
  resources :steps, only: [:new, :index, :show, :update]
  root 'steps#new'
end
